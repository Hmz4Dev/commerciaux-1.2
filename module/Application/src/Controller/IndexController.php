<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController 
{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;
    
   
    public function __construct($entityManager) 
    {
       $this->entityManager = $entityManager;
    }
    
   
    public function indexAction() 
    {
        return new ViewModel();
    }

    
}

