<?php
namespace Comme\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Json\Json;
use Comme\Form\CommeForm;
use Comme\Form\findForm;
use Comme\Entity\Abstract_Model;
use User\Controller\UserController;
use User\Entity\User;
use Comme\Entity\prospects;
use Comme\Entity\NEW_Pays;
use Comme\Entity\contacts;
use Comme\Form\AddContactForm;
use Comme\Form\NewActionForm;
use zend\session\container;

class CommeController extends AbstractActionController
{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * Comme manager.
     * @var Comme\Service\CommeManager
     */
    private $commeManager;
    /**
     * pays manager.
     * @var Comme\Service\paysManager
     */
     private $paysManager;

     /**
      * contacts manager.
      * @var Comme\Service\contactsManager
      */
      private $contactsManager;

      /**
       * visite manager.
       * @var Comme\Service\visiteManager
       */
       private $visiteManager;
       /**
        * prospects manager.
        * @var Comme\Service\prospectsManager
        */
        private $prospectsManager;
    /**
     * Constructor.
     */
    public function __construct($entityManager, $commeManager,$paysManager,$contactsManager,$visiteManager,$prospectsManager)
    {
        $this->entityManager    = $entityManager     ;
        $this->commerManager    = $commeManager      ;
        $this->paysManager      = $paysManager       ;
        $this->contactsManager  = $contactsManager   ;
        $this->visiteManager    = $visiteManager     ;
        $this->prospectsManager = $prospectsManager  ;

    }

   
    public function indexAction()
    {

                $form = new findForm($this->entityManager);

                $data = $this->params()->fromPost();

                $form->setData($data);

                if ($this->getRequest()->isPost())
                 {

                     if($form->isValid())
                     {

                        $data = $form->getData();
                        $container=new container ('entreprise');
                        $container->data=$data['inputsearch'];

                        $this->redirect()->toRoute('comme',array("action"=>"searchresult"));
                     }

                }


        return new ViewModel([
            //'users' => $users,
              'form' => $form
        ]);
    }

    public function searchresultAction()
    {
      //$Id = (int)$this->params()->fromRoute('id', -1);
      $container=new container ('entreprise');
      $Id=$container->data;
        $prospects=$this->prospectsManager->findprospect($Id);
        if ($prospects == null) {

             $this->redirect()->toRoute('comme',array("action"=>"index"));
           return;
       }
    //  $container=new container ('lesentreprises');
    //  $json     =new JsonModel();

      $form = new findForm($this->entityManager);

      $data = $this->params()->fromPost();

      $form->setData($data);



                if ($this->getRequest()->isPost())
                 {
                   if($form->isValid())
                   {
                     $data = $form->getData();
                     $container=new container ('entreprise');
                     $container->data=$data['inputsearch'];
                     $this->redirect()->toRoute('comme',array("action"=>"searchresult"));
                  }

                }

                    return new ViewModel([
                        'prospects' => $prospects,
                          'form' => $form,
                          'paysManager' => $this->paysManager
                    ]);

    }
    public function addcontactAction()
   {
       // Create contact form
       $form = new AddContactForm($this->entityManager);

       // Check if contact has submitted the form
       if ($this->getRequest()->isPost()) {

           // Fill in the form with POST data
           $data = $this->params()->fromPost();

           $form->setData($data);

           // Validate form


                   if($form->isValid() && isset($_POST['Add'])) {

               // Get filtered and validated data
               $data = $form->getData();

               // Add user.
               $prospects = $this->prospectsManager->save($data);

               // Redirect to "commerciaux" page
               return $this->redirect()->toRoute('Commerciaux');
           }
             elseif(isset($_POST['Annuler']))  return $this->redirect()->toRoute('commerciaux');
           }




       return new ViewModel([
               'form' => $form
           ]);
   }

public function addactionnewAction()
{
  // Create add Action form
  $form = new NewActionForm($this->entityManager);

  // Check if user has submitted the form
  if ($this->getRequest()->isPost()) {
      }
      return new ViewModel([
              'form' => $form
          ]);


}
public function goentrpriseAction()
{
  $id=(int) $_POST['id'];
       $reponse=$this->userManager->findbuId($id);
       $data=array();
foreach ($variable as $key => $value) {
$data[$key]=$value;
}


$jsonObject = Zend\Json\Json::encode($data, true);
return $jsonObject;


}
public function fichclientAction()
{   
    $formv = new NewActionForm($this->entityManager);
    $formc = new AddContactForm($this->entityManager);
  $Id = (int)$this->params()->fromRoute('id', -1);
  $contacts  =$this->contactsManager ->findcontacts($Id);
  $visites   =$this->visiteManager   ->findvisites($Id);
  $prospects =$this->entityManager->getRepository(prospects::class)->find($Id);


  if ($this->getRequest()->isPost()) {
            
           
            $data = $this->params()->fromPost();            
            
            $formc->setData($data);
            $formv->setData($data);
            
               if($formc->isValid() && isset($_POST['enregistrer'])) {
                
                
                $data = $formc->getData();
                
                
                $contacts = $this->contactsManager->save($data,$prospects->getid());
                
                
                return $this->redirect()->toRoute('comme',['action'=>'fichclient','id'=>$prospects->getid()]);                
            }elseif ($formv->isValid() && isset($_POST['enregistrerv'])) {
                 $data = $formv->getData();
                
                
                $visites = $this->visiteManager->save($data,$prospects->getid());
                
                
                return $this->redirect()->toRoute('comme',['action'=>'fichclient','id'=>$prospects->getid()]); 
            }
  }







 return new ViewModel([
         'contacts'        => $contacts,
         'prospects'      => $prospects,
         'contactsManager' => $this->contactsManager,
         'visites'          => $visites,
         'visiteManager'   => $this->visiteManager,
         'form'            =>$formv,
         'formc'           =>$formc
         


     ]);

}

}
