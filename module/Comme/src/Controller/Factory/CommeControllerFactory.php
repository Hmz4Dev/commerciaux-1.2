<?php
namespace Comme\Controller\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Comme\Controller\CommeController;
use Comme\Service\CommeManager;
use Comme\Service\paysManager;
use Comme\Service\contactsManager;
use Comme\Service\visiteManager;
use Comme\Service\prospectsManager;


class CommeControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $commeManager = $container->get(CommeManager::class);
        $paysManager = $container->get(paysManager::class);
        $contactsManager = $container->get(contactsManager::class);
        $visiteManager = $container->get(visiteManager::class);
        $prospectsManager = $container->get(prospectsManager::class);

        // Instantiate the controller and inject dependencies
        return new CommeController($entityManager, $commeManager,$paysManager,$contactsManager,$visiteManager,$prospectsManager);
    }
}
