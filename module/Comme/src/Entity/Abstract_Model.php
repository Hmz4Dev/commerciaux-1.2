<?php
namespace Comme\Entity;

use Doctrine\ORM\Mapping as ORM;
use Comme\Entity\contacts;
use DateTime;

abstract class Abstract_Model
{
	/**
	*@ORM\Id
	*@ORM\Column(name="id")
	*@ORM\GeneratedValue
	*/
protected $id;
 /**
     * @ORM\Column(name="CreeLe")
     */
protected $CreeLe;
/**
		* @ORM\Column(name="CreePar")
		*/
protected $CreePar;
/**
		* @ORM\Column(name="ModifieLe")
		*/
protected $ModifieLe;
/**
		* @ORM\Column(name="ModifiePar")
		*/

protected $ModifiePar;
/**
		* @ORM\Column(name="Corbeille")
		*/

protected $Corbeille;


 public $_rec_name = 'id';


 public function __construct(array $options = null)
	{

		if (is_array($options)) {
			$this->setOptions($options);
		}
	}

 public function __set($name, $value)
	{
		$method = 'set'.$name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new \ Exception('Invalid Agent property');
		}
		$this->$method($value);
	}

 public function __get($name)
	{
		$method = 'get' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new \Exception('Invalid Agent property');
		}
		return $this->$method();
	}
	 public function getArrayCopy()
    {
    	return get_object_vars($this);

    }

	public function setOptions(array $options)
	{

	    $str='';
	    //sleep(1);
	    $methods = get_class_methods($this);
		foreach ($options as $key => $value) {
			$method = 'set' . $key; //ucfirst(
			if (in_array($method, $methods)) {
			    $str.=chr(13).chr(10).$method.' -> '.$value;
				$this->$method($value);
			}
		}
		return $this;
	}

	public function getOptions($options=array())
	{
		/*
		 * si le tableau Options est vide, il faut faire attention les propriétés protected $_XXXX doivent exister dans la table de BD
		*/
	    $str='';
		$values = array();
		$methods = get_class_methods($this);
		if(count($options)==0){
			foreach($methods as $method) {
				$field=substr($method,3,strlen($method)-3);
				if(substr($method,0,3)=='get' && $method!='getOptions' &&  property_exists($this,'_'.$field)){
				    $str.=' ; '.$field;
					$values[$field]=$this->$method();
				}
			}
		}
		else
		foreach ($options as $key) {
			$method = 'get' . $key; //ucfirst($key)
			if (in_array($method, $methods)) {
				$values[$key]=$this->$method();
			}
		}
		return $values;
	}

	//--------------------------------------------------------------
	/**
     * Sets ID of this post.
     * @param int $id
     */
	public function setid($id)
	{
		$this->id = $id;
		return $this;
	}
	//--------------------------------------------------------------
	/**
		* Returns ID .
		* @return integer
		*/
	public function getid()
	{
		return $this->id;
	}
	//--------------------------------------------------------------
	/**
     * Sets CreeLe of this post.
     * @param string $CreeLe
     */
	public function setCreeLe($CreeLe)
	{

			$this->CreeLe =$CreeLe ;


	}
	//--------------------------------------------------------------
	/**
     * Returns CreeLe of this post.
     * @return string
     */
	public function getCreeLe()
	{
		if(!isset($this->CreeLe)) return null;
		return $this->CreeLe;
	}
  	//--------------------------------------------------------------
	public function setCreePar($CreePar)
	{
		$this->_CreePar = $CreePar;
		return $this;
	}
	//--------------------------------------------------------------
	public function getCreePar()
	{
		return $this->_CreePar;
	}
	//--------------------------------------------------------------
	public function setModifieLe($ModifieLe)
	{
		if($ModifieLe!='' && $ModifieLe!=null && $ModifieLe!='0000-00-00 00:00:00')
			$this->_ModifieLe = new DateTime($ModifieLe) ;
		else $this->_ModifieLe = null;
		return $this;
	}
	//--------------------------------------------------------------
	public function getModifieLe($format='Y-m-d H:i:s')
	{
		if(!isset($this->_ModifieLe)) return null;
		return $this->_ModifieLe->format($format);
	}
	//--------------------------------------------------------------
	public function setModifiePar($ModifiePar)
	{
		$this->_ModifiePar = $ModifiePar;
		return $this;
	}
	//--------------------------------------------------------------
	public function getModifiePar()
	{
		return $this->_ModifiePar;
	}
	//----------------------------------------------------------------
	//--------------------------------------------------------------
	public function setCorbeille($Corbeille)
	{
		$this->_Corbeille = $Corbeille;
		return $this;
	}
	//--------------------------------------------------------------
	public function getCorbeille()
	{
		return $this->_Corbeille;
	}
	//----------------------------------------------------------------

/*	 public function gettypecAsString($code)
    {

    $typec=$this->entityManager->getRepository(typec::class)->findAll();
    $selectData = array();
        $selectData[0] =" ";
        foreach ($typec as $res) {
            $selectData[$res->getcode()] = $res->gettypecs();
        }
        return $selectData[$code];

    }   */
};
