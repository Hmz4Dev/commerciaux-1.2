<?php
namespace Comme\Entity;

use Comme\Entity\Abstract_Model;
use Doctrine\ORM\Mapping as ORM;
/**
 * This class represents a registered Commerciaux.
 * @ORM\Entity()
 * @ORM\Table(name="Commerciaux")
 */
class Commerciaux
{
     /**
  *@ORM\Id
  *@ORM\Column(name="Num")
  *@ORM\GeneratedValue
  */
     protected $Num;
     /**
     * @ORM\Column(name="NumAssistante")
     */
     protected $NumAssistante;
     /**
     * @ORM\Column(name="Civilite")
     */
     protected $Civilite;
     /**
     * @ORM\Column(name="Nom")
     */
     protected $Nom;
     /**
     * @ORM\Column(name="Prenom")
     */
     protected $Prenom;
     /**
     * @ORM\Column(name="Email")
     */
     protected $Email;
     /**
     * @ORM\Column(name="Actif")
     */
     protected $Actif;
     /**
     * @ORM\Column(name="Compteur")
     */
     protected $Compteur;
     /**
     * @ORM\Column(name="droits_d_acces")
     */
     protected $droits_d_acces;
     /**
     * @ORM\Column(name="Login")
     */
     protected $Login;
     /**
     * @ORM\Column(name="Password")
     */
     protected $Password;
     /**
     * @ORM\Column(name="LastLogin")
     */
     protected $LastLogin;
     /**
     * @ORM\Column(name="adm_fichetec")
     */
     protected $adm_fichetec;
     /**
     * @ORM\Column(name="zonegeo")
     */
     protected $zonegeo;


     public function __construct(array $options = null)
         {

            $this->_rec_name = 'nom';
            parent::__construct($options);
          }

    /**
     * Returns Id.
     * @return string
     */

	public function getNum(){ return $this->Num; }
     /**
     * Sets Id.
     * @param string $Num
     */
   	public function setNum($Num){$this->Num=$Num; return $this; }

    /**
     * Returns NumAssistante.
     * @return string
     */

   	public function getNumAssistante(){ return $this->NumAssistante; }
    /**
     * Sets NumAssistante.
     * @param string $NumAssistante
     */
   	public function setNumAssistante($NumA){$this->NumAssistante=$NumA; return $this; }
    /**
     * Sets prenom.
     * @param string $prenom
     */

   	public function setPrenom($Prenom) { $this->prenom = $Prenom; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns Prenom.
     * @return string
     */

    public function getPrenom() { return $this->Prenom; }
    //--------------------------------------------------------------
    /**
     * Sets Nom.
     * @param string $Nom
     */
    public function setNom($nom) { $this->Nom = $Nom; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns Nom.
     * @return string
     */

    public function getNom() { return $this->Nom; }
    //--------------------------------------------------------------
    /**
     * Sets Email.
     * @param string $Email
     */
    public function setEmail($email) { $this->Email = $email; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns Email.
     * @return string
     */

    public function getEmail() { return $this->Email; }
    //--------------------------------------------------------------
/**
     * Sets Civilite.
     * @param string $Civilite
     */
    public function setCivilite($civilite) { $this->Civilite = $civilite; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns Civilite.
     * @return string
     */

    public function getCivilite() { return $this->Civilite; }
    //--------------------------------------------------------------
     /**
     * Sets droits_d_acces.
     * @param string $droits_d_acces
     */
    public function setdroits_d_acces($droits_d_acces) { $this->droits_d_acces = $droits_d_acces; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns droits_d_acces.
     * @return string
     */

    public function getdroits_d_acces() { return $this->droits_d_acces; }
    //--------------------------------------------------------------
    /**
     * Sets Login.
     * @param string $Login
     */
    public function setLogin($Login) { $this->Login = $Login; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns Login.
     * @return string
     */

    public function getLogin() { return $this->Login; }
    //--------------------------------------------------------------
     /**
     * Sets Password.
     * @param string $Password
     */
    public function setPassword($Password) { $this->Password = $Password; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns Password.
     * @return string
     */

    public function getPassword() { return $this->Password; }

    //--------------------------------------------------------------
    /**
     * Sets Compteur.
     * @param string $Compteur
     */
    public function setCompteur($Compteur) { $this->Compteur = $Compteur; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns Compteur.
     * @return string
     */

    public function getCompteur() { return $this->Compteur; }
    //--------------------------------------------------------------
     /**
     * Sets LastLogin.
     * @param string $LastLogin
     */
    public function setLastLogin($LastLogin) { $this->LastLogin = $LastLogin; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns LastLogin.
     * @return string
     */

    public function getLastLogin() { return $this->LastLogin; }
    //--------------------------------------------------------------
     /**
     * Sets adm_fichetec.
     * @param string $adm_fichetec
     */
    public function setadm_fichetec($spe1) { $this->adm_fichetec = $spe1; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns adm_fichetec.
     * @return string
     */

    public function getadm_fichetec() { return $this->adm_fichetec; }
    //--------------------------------------------------------------
     /**
     * Sets zonegeo.
     * @param string $zonegeo
     */
    public function setzonegeo($spe2) { $this->zonegeo = $spe2; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns zonegeo.
     * @return string
     */

    public function getazonegeo() { return $this->zonegeo; }
    //--------------------------------------------------------------
      /**
     * Sets Actif.
     * @param string $Actif
     */
    public function setActif($Actif) { $this->Actif = $Actif; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns Actif.
     * @return string
     */

    public function getActif() { return $this->Actif; }
    //--------------------------------------------------------------


     }
