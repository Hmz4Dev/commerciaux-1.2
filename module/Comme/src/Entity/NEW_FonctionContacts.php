<?php
namespace Comme\Entity;

use Comme\Entity\Abstract_Model;
use Doctrine\ORM\Mapping as ORM;
/**
 * This class represents a registered NEW_FonctionContacts.
 * @ORM\Entity()
 * @ORM\Table(name="NEW_FonctionContacts")
 */
class NEW_FonctionContacts extends Abstract_Model
{

      /**
     * @ORM\Column(name="Designation")
     */
     public $Designation;


     public function __construct(array $options = null)
         {

          $this->_rec_name = 'nom';
        parent::__construct($options);
        }


   	  /**
     * Returns Designation.
     * @return string
     */
   	 public function getDesignation(){return $this->Designation; }
   	  /**
     * Sets Designation.
     * @param string $Designation
     */
	 public function setDesignation($Designation){$this->Designation=$Designation; return $this; }



}
