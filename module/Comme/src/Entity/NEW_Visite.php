<?php
namespace Comme\Entity;

use Comme\Entity\Abstract_Model;
use Doctrine\ORM\Mapping as ORM;
/**
 * This class represents a single post in a blog.
 * @ORM\Entity(repositoryClass="\Comme\Repository\NEW_VisiteRepository")
 * @ORM\Table(name="NEW_Visite")
 */

class NEW_Visite extends Abstract_Model
{




     /**
     * @ORM\Column(name="idProspect")
     */
    protected $idProspect;
     /**
     * @ORM\Column(name="Destinataires")
     */
    protected $Destinataires;
   /**
     * @ORM\Column(name="idContact1")
     */
    protected $idContact1;
    /**
     * @ORM\Column(name="idContact2")
     */
    protected $idContact2;
    /**
     * @ORM\Column(name="idContact3")
     */
    protected $idContact3;



    /**
     * @ORM\Column(name="Commentaire")
     */
    protected $Commentaire;
    /**
     * @ORM\Column(name="idAction")
     */
    protected $idAction;

    public function __construct(array $options = null)
         {

	        $this->_rec_name = 'Destinataires';
		    parent::__construct($options);
	      }

	          //--------------------------------------------------------------
  //  public function setprospect($prospect) { $this->_prospect = $prospect; return $this; }
    //--------------------------------------------------------------
  //  public function getprospect() { return $this->_prospect; }

    //--------------------------------------------------------------
          /**
     * Sets idProspect.
     * @param string $idProspect
     */
    public function setidProspect($idProspect) { $this->idProspect = $idProspect; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns idProspect.
     * @return string
     */
    public function getidProspect() { return $this->idProspect; }






    //--------------------------------------------------------------
    /**
     * Sets Destinataires.
     * @param string $Destinataires
     */
    public function setDestinataires($Destinataires) { $this->Destinataires = $Destinataires; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns Destinataires.
     * @return string
     */
    public function getDestinataires() { return $this->Destinataires; }
    //--------------------------------------------------------------
    /**
     * Sets idContact1.
     * @param string $idContact1
     */
    public function setidContact1($idContact1) { $this->idContact1 = $idContact1; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns idContact1.
     * @return string
     */
    public function getidContact1() { return $this->idContact1; }
    //--------------------------------------------------------------
    /**
     * Sets idContact2.
     * @param string $idContact2
     */
    public function setidContact2($idContact2) { $this->idContact2 = $idContact2; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns idContact2.
     * @return string
     */
    public function getidContact2() { return $this->idContact2; }

    //--------------------------------------------------------------
    /**
     * Sets Commentaire.
     * @param string $Commentaire
     */
    public function setCommentaire($Commentaire) { $this->Commentaire = $Commentaire; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns Commentaire.
     * @return string
     */
    public function getCommentaire() { return $this->Commentaire; }
    //--------------------------------------------------------------
    /**
     * Sets idAction.
     * @param string $idAction
     */
    public function setidAction($idAction) { $this->idAction = $idAction; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns idAction.
     * @return string
     */
    public function getidAction() { return $this->idAction; }
    //--------------------------------------------------------------
    /**
     * Sets idContact3.
     * @param string $idContact3
     */
    public function setidContact3($idContact3) { $this->idContact3 = $idContact3; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns idContact3.
     * @return string
     */
    public function getidContact3() { return $this->idContact3; }

	};
