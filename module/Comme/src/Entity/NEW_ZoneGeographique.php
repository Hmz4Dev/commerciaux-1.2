<?php
namespace Comme\Entity;

use Comme\Entity\Abstract_Model;
use Doctrine\ORM\Mapping as ORM;
/**
 * This class represents a registered NEW_ZoneGeographique.
 * @ORM\Entity()
 * @ORM\Table(name="NEW_ZoneGeographique")
 */
class NEW_ZoneGeographique extends Abstract_Model
{

      /**
     * @ORM\Column(name="Designation")
     */
     public $Designation;


     public function __construct(array $options = null)
         {

          $this->_rec_name = 'nom';
        parent::__construct($options);
        }


   	  /**
     * Returns Designation.
     * @return string
     */
   	 public function getDesignation(){return $this->Designation; }
   	  /**
     * Sets Designation.
     * @param string $Designation
     */
	 public function setTypecs($Designation){$this->Designation=$Designation; return $this; }



}
