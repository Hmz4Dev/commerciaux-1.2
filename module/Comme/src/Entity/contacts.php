<?php
namespace Comme\Entity;

require_once 'Abstract_Model.php';
use Comme\Entity\Abstract_Model;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * This class represents a single post in a blog.
 * @ORM\Entity(repositoryClass="\Comme\Repository\contactsRepository")
 * @ORM\Table(name="contacts")
 */

class contacts extends Abstract_Model
{

     const Referent_Non      = 0; 
     const Referent_Oui      = 2; 



     /**
     * @ORM\Column(name="idProspect")
     */
    protected $idProspect;
     /**
     * @ORM\Column(name="nom")
     */
    protected $nom;
   /**
     * @ORM\Column(name="adr1")
     */
    protected $adr1;
    /**
     * @ORM\Column(name="adr2")
     */
    protected $adr2;
    /**
     * @ORM\Column(name="adr3")
     */
    protected $adr3;

     /**
      * @ORM\Column(name="adr4")
      */
     protected $adr4;

    /**
     * @ORM\Column(name="ville")
     */
    protected $ville;
    /**
     * @ORM\Column(name="telephone")
     */
    protected $telephone;

    /**
     * @ORM\Column(name="email")
     */
    protected $email;


    /**
     * @ORM\Column(name="mobile")
     */
    protected $mobile;
    /**
     * @ORM\Column(name="fonction")
     */
    protected $fonction;

     /**
     * @ORM\Column(name="civilite")
     */
    protected $civilite;

     /**
     * @ORM\Column(name="cp")
     */
    protected $cp;

     /**
     * @ORM\Column(name="spe1")
     */
    protected $spe1;

    /**
    * @ORM\Column(name="prenom")
    */
    protected $prenom;

        public function __construct(array $options = null)
         {

	        $this->_rec_name = 'nom';
		    parent::__construct($options);
	      }

	          //--------------------------------------------------------------
  //  public function setprospect($prospect) { $this->_prospect = $prospect; return $this; }
    //--------------------------------------------------------------
  //  public function getprospect() { return $this->_prospect; }

    //--------------------------------------------------------------
          /**
     * Sets idProspect.
     * @param string $idProspect
     */
    public function setidProspect($idProspect) { $this->idProspect = $idProspect; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns idProspect.
     * @return string
     */
    public function getidProspect() { return $this->idProspect; }

    //--------------------------------------------------------------
    /**
     * Sets nom.
     * @param string $nom
     */
    public function setnom($nom) { $this->nom = $nom; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns nom.
     * @return string
     */
    public function getnom() { return $this->nom; }
    //--------------------------------------------------------------
    /**
     * Sets adr1.
     * @param string $adr1
     */
    public function setadr1($adr1) { $this->adr1 = $adr1; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns adr1.
     * @return string
     */
    public function getadr1() { return $this->adr1; }
    //--------------------------------------------------------------
    /**
     * Sets adr2.
     * @param string $adr2
     */
    public function setadr2($adr2) { $this->adr2 = $adr2; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns adr2.
     * @return string
     */
    public function getadr2() { return $this->adr2; }

    //--------------------------------------------------------------
    /**
     * Sets ville.
     * @param string $ville
     */
    public function setville($ville) { $this->ville = $ville; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns ville.
     * @return string
     */
    public function getville() { return $this->ville; }
    //--------------------------------------------------------------
    /**
     * Sets telephone.
     * @param string $telephone
     */
    public function settelephone($telephone) { $this->telephone = $telephone; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns telephone.
     * @return string
     */
    public function gettelephone() { return $this->telephone; }
    //--------------------------------------------------------------
   /* public function setfax($fax) { $this->_fax = $fax; return $this; }
    //--------------------------------------------------------------
    public function getfax() { return $this->_fax; }
   */ //--------------------------------------------------------------
    /**
     * Sets email.
     * @param string $email
     */
    public function setemail($email) { $this->email = $email; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns email.
     * @return string
     */
    public function getemail() { return $this->email; }
    //--------------------------------------------------------------
    /**
     * Sets zonegeo.
     * @param string $zonegeo
     */
    public function setzonegeo($zonegeo) { $this->zonegeo = $zonegeo; return $this; }
    //--------------------------------------------------------------
    /**
     * Sets mobile.
     * @param string $mobile
     */
    public function setmobile($mobile) { $this->mobile = $mobile; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns mobile.
     * @return string
     */
    public function getmobile() { return $this->mobile; }
    //--------------------------------------------------------------
    /**
     * Sets fonction.
     * @param string $fonction
     */
    public function setfonction($fonction) { $this->fonction = $fonction; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns fonction.
     * @return string
     */
    public function getfonction() { return $this->fonction; }
    //--------------------------------------------------------------
    /**
     * Sets civilite.
     * @param string $civilite
     */
    public function setcivilite($civilite) { $this->civilite = $civilite; return $this; }
    //--------------------------------------------------------------
     /**
     * Returns civilite.
     * @return string
     */
    public function getcivilite() { return $this->civilite; }
    //--------------------------------------------------------------
    /**
     * Sets adr3.
     * @param string $adr3
     */
    public function setadr3($adr3) { $this->adr3 = $adr3; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns adr3.
     * @return string
     */
    public function getadr3() { return $this->adr3; }
    //--------------------------------------------------------------
    /**
     * Sets cp.
     * @param int $cp
     */
    public function setcp($cp) { $this->cp = $cp; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns spe1.
     * @return int
     */
    public function getspe1() { return $this->spe1; }
     //--------------------------------------------------------------
     /**
     * Sets spe1.
     * @param int $spe1
     */
    public function setspe1($spe1) { $this->spe1 = $spe1; return $this; }

    public static function getReferentList()
    {
        return [
            self::Referent_Non => 'Non',
            self::Referent_Oui => 'Oui'
        ];
    }

    /**
     * Returns user Referent as string.
     * @return string
     */
    public function getReferentAsString()
    {
        $list = self::getProfilList();
        if (isset($list[$this->spe1]))
            return $list[$this->spe1];

        return 'Unknown';
    }

 
    //--------------------------------------------------------------
    /**
     * Returns cp.
     * @return int
     */
    public function getcp() { return $this->cp; }
     //--------------------------------------------------------------
     /**
      * Sets prenom.
      * @param string $prenom
      */
     public function setprenom($prenom) { $this->prenom= $prenom; return $this; }
     //--------------------------------------------------------------
     /**
      * Returns prenom.
      * @return string
      */
     public function getprenom() { return $this->prenom; }
      //--------------------------------------------------------------
   // public function setstatut($statut) { $this->_statut = $statut; return $this; }
    //--------------------------------------------------------------
    //public function getstatut() { return $this->_statut; }
    //--------------------------------------------------------------

	};
