<?php
namespace Comme\Entity;

use Comme\Entity\Abstract_Model;
use Doctrine\ORM\Mapping as ORM;
use Comme\Entity\typec;

/**
 * This class represents a single post in a blog.
 * @ORM\Entity(repositoryClass="\Comme\Repository\prospectsRepository")
 * @ORM\Table(name="prospects")
 */

class prospects extends Abstract_Model
{




     /**
     * @ORM\Column(name="typec")
     */
    protected $typec;
     /**
     * @ORM\Column(name="nom")
     */
    protected $nom;
   /**
     * @ORM\Column(name="address1")
     */
    protected $address1;
    /**
     * @ORM\Column(name="address2")
     */
    protected $address2;
    /**
     * @ORM\Column(name="address3")
     */
    protected $address3;

     /**
      * @ORM\Column(name="address4")
      */
     protected $address4;
     /**
      * @ORM\Column(name="postcode")
      */

   protected $postcode;
    /**
     * @ORM\Column(name="ville")
     */
    protected $ville;
    /**
     * @ORM\Column(name="telephone")
     */
    protected $telephone;

    /**
     * @ORM\Column(name="email")
     */
    protected $email;

    /**
     * @ORM\Column(name="zonegeo")
     */
    protected $zonegeo;
    /**
     * @ORM\Column(name="representant")
     */
    protected $representant;

     /**
     * @ORM\Column(name="fid")
     */
    protected $fid;
     /**
     * @ORM\Column(name="population")
     */
    protected $population;
     /**
     * @ORM\Column(name="Pays")
     */
    protected $Pays;

     /**
     * @ORM\Column(name="Actif")
     */
    protected $Actif;

     /**
     * @ORM\Column(name="CodeGestion")
     */
    protected $CodeGestion;

     /**
     * @ORM\Column(name="DT")
     */
    protected $DT;
     /**
     * @ORM\Column(name="nextaction")
     */
    protected $nextaction;
     /**
     * @ORM\Column(name="nextyear")
     */
    protected $nextyear;

     /**
     * @ORM\Column(name="nextaction2")
     */
    protected $nextaction2;
     /**
     * @ORM\Column(name="nextyear2")
     */
    protected $nextyear2;
     /**
     * @ORM\Column(name="nextaction3")
     */
    protected $nextaction3;
     /**
     * @ORM\Column(name="nextyear3")
     */
    protected $nextyear3;
     /**
     * @ORM\Column(name="ValeurDossier")
     */
    protected $ValeurDossier;
    /**
    * @ORM\Column(name="statut")
    */
    protected $statut;
    /**
    * @ORM\Column(name="PourcentReussite")
    */
    protected $PourcentReussite;

        public function __construct(array $options = null)
         {

	        $this->_rec_name = 'nom';
		    parent::__construct($options);
	      }

	          //--------------------------------------------------------------
  //  public function setprospect($prospect) { $this->_prospect = $prospect; return $this; }
    //--------------------------------------------------------------
  //  public function getprospect() { return $this->_prospect; }

    //--------------------------------------------------------------
          /**
     * Sets typec.
     * @param string $typec
     */
    public function settypec($typec) { $this->typec = $typec; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns typec.
     * @return string
     */
    public function gettypec() { return $this->typec; }






    //--------------------------------------------------------------
    /**
     * Sets nom.
     * @param string $nom
     */
    public function setnom($nom) { $this->nom = $nom; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns nom.
     * @return string
     */
    public function getnom() { return $this->nom; }
    //--------------------------------------------------------------
    /**
     * Sets address1.
     * @param string $address1
     */
    public function setaddress1($address1) { $this->address1 = $address1; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns address1.
     * @return string
     */
    public function getaddress1() { return $this->address1; }
    //--------------------------------------------------------------
    /**
     * Sets address2.
     * @param string $address2
     */
    public function setaddress2($address2) { $this->address2 = $address2; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns address2.
     * @return string
     */
    public function getaddress2() { return $this->address2; }
    //--------------------------------------------------------------
    /**
     * Sets postcode.
     * @param integer $postcode
     */
    public function setpostcode($postcode) { $this->postcode = $postcode; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns postcode.
     * @return integer
     */
    public function getpostcode() { return $this->postcode; }
    //--------------------------------------------------------------
    /**
     * Sets ville.
     * @param string $ville
     */
    public function setville($ville) { $this->ville = $ville; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns ville.
     * @return string
     */
    public function getville() { return $this->ville; }
    //--------------------------------------------------------------
    /**
     * Sets telephone.
     * @param integer $telephone
     */
    public function settelephone($telephone) { $this->telephone = $telephone; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns telephone.
     * @return integer
     */
    public function gettelephone() { return $this->telephone; }
    //--------------------------------------------------------------
   /* public function setfax($fax) { $this->_fax = $fax; return $this; }
    //--------------------------------------------------------------
    public function getfax() { return $this->_fax; }
   */ //--------------------------------------------------------------
    /**
     * Sets email.
     * @param string $email
     */
    public function setemail($email) { $this->email = $email; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns email.
     * @return string
     */
    public function getemail() { return $this->email; }


    //--------------------------------------------------------------
    /**
     * Sets zonegeo.
     * @param integer $zonegeo
     */
    public function setzonegeo($zonegeo) { $this->zonegeo = $zonegeo; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns zonegeo.
     * @return integer
     */
    public function getzonegeo() { return $this->zonegeo; }
    //--------------------------------------------------------------
    /**
     * Sets representant.
     * @param string $representant
     */
    public function setrepresentant($representant) { $this->representant = $representant; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns representant.
     * @return string
     */
    public function getrepresentant() { return $this->representant; }


    //--------------------------------------------------------------

    /**
     * Sets fid.
     * @param integer $fid
     */
    public function setfid($fid) { $this->fid = $fid; return $this; }
    //--------------------------------------------------------------
     /**
     * Returns fid.
     * @return integer
     */
    public function getfid() { return $this->fid; }
    //--------------------------------------------------------------
    
    /**
     * Sets Pays.
     * @param string $Pays
     */
    public function setPays($Pays) { $this->Pays = $Pays; return $this; }
    //--------------------------------------------------------------
     /**
     * Returns Pays.
     * @return string
     */
    public function getPays() { return $this->Pays; }
    //--------------------------------------------------------------
    /**
     * Sets Actif.
     * @param string $Actif
     */
    public function setActif($Actif) { $this->Actif = $Actif; return $this; }
    //--------------------------------------------------------------
     /**
     * Returns Actif.
     * @return string
     */
    public function getActif() { return $this->Actif; }
    //--------------------------------------------------------------
    /**
     * Sets CodeGestion.
     * @param string $CodeGestion
     */
    public function setCodeGestion($CodeGestion) { $this->CodeGestion = $CodeGestion; return $this; }
    //--------------------------------------------------------------
     /**
     * Returns CodeGestion.
     * @return string
     */
    public function getCodeGestion() { return $this->CodeGestion; }
    //--------------------------------------------------------------

    /**
     * Sets address3.
     * @param string $address3
     */
    public function setaddress3($address3) { $this->address3 = $address3; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns address3.
     * @return string
     */
    public function getaddress3() { return $this->address3; }

    //--------------------------------------------------------------

    /**
     * Sets DT.
     * @param string $DT
     */
    public function setDT($DT) { $this->DT = $DT; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns DT.
     * @return string
     */
    public function getDT() { return $this->DT; }
    //--------------------------------------------------------------
    /**
     * Sets nextaction.
     * @param string $nextaction
     */
    public function setnextaction($nextaction) { $this->nextaction = $nextaction; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns nextaction.
     * @return string
     */
    public function getnextaction() { return $this->nextaction; }
    //--------------------------------------------------------------
    /**
     * Sets nextyear.
     * @param string $nextyear
     */
    public function setnextyear($nextyear) { $this->nextyear = $nextyear; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns nextyear.
     * @return string
     */
    public function getnextyear() { return $this->nextyear; }
    //--------------------------------------------------------------

    /**
     * Sets nextaction2.
     * @param string $nextaction2
     */
    public function setnextaction2($nextaction2) { $this->nextaction2 = $nextaction2; return $this; }
    //--------------------------------------------------------------
     /**
     * Returns nextaction2.
     * @return string
     */
    public function getnextaction2() { return $this->nextaction2; }
    //--------------------------------------------------------------
    /**
     * Sets nextyear2.
     * @param string $nextyear2
     */
    public function setnextyear2($nextyear2) { $this->nextyear2 = $nextyear2; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns nextyear2.
     * @return string
     */
    public function getnextyear2() { return $this->nextyear2; }
    //--------------------------------------------------------------
    /**
     * Sets nextaction3.
     * @param string $nextaction3
     */
    public function setnextaction3($nextaction3) { $this->nextaction3 = $nextaction3; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns nextaction3.
     * @return string
     */
    public function getnextaction3() { return $this->nextaction3; }
    //--------------------------------------------------------------
    /**
     * Sets nextyear3.
     * @param string $nextyear3
     */
    public function setnextyear3($nextyear3) { $this->nextyear3 = $nextyear3; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns nextyear3.
     * @return string
     */
    public function getnextyear3() { return $this->nextyear3; }
    //--------------------------------------------------------------
    /**
     * Sets ValeurDossier.
     * @param string $ValeurDossier
     */
    public function setValeurDossier($ValeurDossier) { $this->ValeurDossier = $ValeurDossier; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns ValeurDossier.
     * @return string
     */
    public function getValeurDossier() { return $this->ValeurDossier; }
    //--------------------------------------------------------------
    /**
     * Sets CreePar.
     * @param string $CreePar
     */
    public function setCreePar($CreePar) { $this->CreePar = $CreePar; return $this; }
    //--------------------------------------------------------------
    /**
     * Returns CreePar.
     * @return string
     */
    public function getCreePar() { return $this->CreePar; }
     //--------------------------------------------------------------
     //--------------------------------------------------------------
     /**
      * Sets PourcentReussite.
      * @param string $PourcentReussite
      */
     public function setPourcentReussite($PourcentReussite) { $this->PourcentReussite= $PourcentReussite; return $this; }
     //--------------------------------------------------------------
     /**
      * Returns PourcentReussite.
      * @return string
      */
     public function getPourcentReussite() { return $this->PourcentReussite; }
      //--------------------------------------------------------------
   // public function setstatut($statut) { $this->_statut = $statut; return $this; }
    //--------------------------------------------------------------
    //public function getstatut() { return $this->_statut; }
    //--------------------------------------------------------------

	};
