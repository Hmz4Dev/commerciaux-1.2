<?php
namespace Comme\Entity;

use Comme\Entity\Abstract_Model;
use Doctrine\ORM\Mapping as ORM;
/**
 * This class represents a registered typec.
 * @ORM\Entity()
 * @ORM\Table(name="typec")
 */
class typec
{
	  /**
  *@ORM\Id
  *@ORM\Column(name="code")
  *@ORM\GeneratedValue
  */
	    protected $code;
	 /**
     * @ORM\Column(name="typec")
     */
     protected $typec;

     public function __construct(array $options = null)
         {

          $this->_rec_name = 'nom';
        parent::__construct($options);
        }

     /**
     * Returns Id.
     * @return string
     */

	   public function getCode(){ return $this->code; }
	  /**
     * Sets Id.
     * @param string $code
     */
   	 public function setCode($code){$this->code=$code; return $this; }
   	 /**
     * Returns typec.
     * @return string
     */

   	 public function getTypecs(){return $this->typec; }
   	  /**
     * Sets typec.
     * @param string $typec
     */
	 public function setTypecs($typec){$this->typec=$typec; return $this; }


}
