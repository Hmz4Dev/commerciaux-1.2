<?php

namespace Comme\Form;

use Zend\Form\Form;
use Zend\Form\fieldset;
use Zend\InputFilter\InputFilter;
use Comme\Entity\NEW_FonctionContacts;
//use Comme\Validator\SocitExistsValidator;
//use  Comme\Validator\ChampValidator;


class AddContactForm extends Form {


  	    private $entityManager=null;
  	    private $prospects=null;


public function __construct($entityManager=null,$prospects=null)
{

    parent::__construct('addcontactform');
    $this->setAttribute('method', 'post');

    $this->entityManager = $entityManager;
    $this->prospects = $prospects;
    $this->addElements();
    $this->addInputFilter();

}
 public function addElements(){

    $this->add(array(
             'name' => 'id',
             'type' => 'Hidden',
         ));
    
     $this->add([
            'type'  => 'select',
            'name' => 'civilite',
            'attributes'=>['style' => 'width: 22%;','class'=>'form-control',],
            'options' => [
                'label' => 'Civilité :',
                'value_options' => [
                    0 => '',
                    2 => 'M',
                    1 => 'Mme',
                ]
            ],
        ]);

        $this->add(array(
            'name'=>'nom',
      'attributes'=>array(
                   'type'=>'text',
                           'size'  => '30',
                           'style' => 'width: 30%;',
                           'class'=>'form-control',
                         ),
            'options'=>array(
                    'label'=>'Nom :'
                            )
             ));

    $this->add(array(
            'name'=>'prenom',
      'attributes'=>array(
                   'type'=>'text',
                           'size'  => '30',
                           'style' => 'width: 30%;',
                           'class'=>'form-control',
                         ),
            'options'=>array(
                    'label'=>'Prenom :'
                            )
             ));         
	 $this->add(array(
            'name'=>'telephone',
      'attributes'=>array(
                   'type'=>'Int',
                           'size'  => '30',
                           'style' => 'width: 30%;',
                            'class'=>'form-control',
                             ),
            'options'=>array(
                    'label'=>'Téléphone :'
                            )
             ));
     



      $this->add(array(
            'name'=>'mobile',
      'attributes'=>array(
                   'type'=>'Int',
                           'size'  => '30',
                           'style' => 'width: 30%;',
                            'class'=>'form-control',
                             ),
            'options'=>array(
                    'label'=>'Mobile :'
                            )
             ));

        
         $this->add(array(
            'name'=>'email',
      'attributes'=>array(
                   'type'=>'Email',
                           'size'  => '30',
                           'style' => 'width: 100%;',
                            'class'=>'form-control',
                             ),
            'options'=>array(
                    'label'=>'Email :'
                            )
             ));

         $this->add([
               'type' => 'Select',
               'name' => 'fonction',
               'options' => [
               'label'   => 'Fonction :',
               'value_options' =>$this->getfonction(),
                          ],
               'attributes' =>[
                                 'value' => '1', 
                                 'style' => 'width: 100%;',
                                 'class'=>'form-control',                       
                                ],

                               ]);    

	   $this->add(array(
            'name'=>'adr1',
      'attributes'=>array(
                   'type'=>'textarea',
                             'rows'        =>'1',
                            
                             'style'       =>'resize:none',
                             'style' => 'width: 100%;',
                              'class'=>'form-control',
                        
                             ),
            'options'=>array(
                    'label'=>'Adresse :'
                            )
             ));

     $this->add(array(
            'name'=>'adr2',
      'attributes'=>array(
                   'type'=>'textarea',
                            'rows'        =>'2',
                             'cols'        =>'50',
                             'style'       =>'resize:none',
                           
                             ),
            'options'=>array(
                    'label'=>'Adresse2 :'
                            )
             ));

     $this->add(array(
            'name'=>'adr3',
      'attributes'=>array(
                   'type'=>'textarea',
                            'rows'        =>'2',
                             'cols'        =>'50',
                             'style'       =>'resize:none',
                         
                             ),
            'options'=>array(
                    'label'=>'Adresse3 :'
                            )
             ));

       $this->add(array(
            'name'=>'cp',
      'attributes'=>array(
                   'type'=>'Int',
                   'style' => 'width: 30%;',
                    'class'=>'form-control',
                         ),
            'options'=>array(
                    'label'=>'Cp :',


                            )
             ));


  $this->add(array(
            'name'=>'ville',
      'attributes'=>array(
                   'type'=>'text',
                           'size'  => '30',
                           'style' => 'width: 50%;',
                            'class'=>'form-control',
                      
                             ),
            'options'=>array(
                    'label'=>'Ville :'
                            )
             ));

 $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'spe1',
            'options' => array(
                'label' => 'Référent :',
                'value_options' => array(
                    '0' => 'Non',
                    '1' => 'Oui',
                ),
            ),
            'attributes' => array(
                'value' => '0' //set checked to '1'
            )
        ));
              

    
 

				   $this->add(array(
				   'name'=>'enregistrer',
				   'type'=>'submit',
				   'attributes'=>array(
				        'value'=>'Enregistrer',
				        'class'=>'bbtn btn-primary btn-sm',
				        'size'=>30,
				        'id' => 'submitbutton'

						)

			     	   ));
				    $this->add(array(
				   'name'=>'annuler',
				   'type'=>'submit',
				   'attributes'=>array(
				        'value'=>'Annuler',
				        'class'=>'btn btn-default btn-sm',
						'id' => 'submitbutton'
						)

				   ));
                    }

  private function addInputFilter()
  {
      $inputFilter=new InputFilter();
      $this->setInputFilter($inputFilter);

  /*    $inputFilter->add([
          'name'     =>'nom',
          'required' =>true,
          'filters'  =>[
              ['name'=>'StringTrim'],
          ],
          'validators'=>[
                           [
                        'name' => SocitExistsValidator::class,
                        'options' => [
                            'entityManager' => $this->entityManager,
                            'prospects' => $this->prospects
                        ],
                    ],


          ],
      ]);
*/
  /*     $inputFilter->add([
          'name'     =>'typec',
          'required' =>true,
           'filters'  => [
                    ['name' => 'ToInt'],
                ],
                'validators' => [
                    [
                    'name'=>'InArray',
                     'options'=>[
                             'haystack'=>[1, 2],



                     ]

                   ],


               [
                        //'name' => ChampValidator::class,
                        'options' => [
                            'entityManager' => $this->entityManager,
                            'prospects' => $this->prospects
                        ],
                    ],
                ],
      ]);
/*
       $inputFilter->add([
          'name'     =>'fid',
          'required' =>true,
          'filters'  =>[
              ['name'=>'StringTrim'],
          ],
          'validators'=>[
                       [
                        'name' => ChampValidator::class,
                        'options' => [
                            'entityManager' => $this->entityManager,
                            'prospects' => $this->prospects
                        ],
                    ],
          ]

      ]);

       $inputFilter->add([
          'name'     =>'representant',
          'required' =>true,
          'filters'  =>[
              ['name'=>'StringTrim'],
          ],
          'validators'=>[
               [
                        'name' => ChampValidator::class,
                        'options' => [
                            'entityManager' => $this->entityManager,
                            'prospects' => $this->prospects
                        ],
                    ],
          ],
      ]);
      */
  }

  /*
public function gettypec()
{

	$typec=$this->entityManager->getRepository(typec::class)->findAll();
	$selectData = array();
        $selectData[0] =" ";
        foreach ($typec as $res) {
            $selectData[$res->getcode()] = $res->gettypecs();
        }
        return $selectData;
}
*/
public function getfonction()
{

  $fonction=$this->entityManager->getRepository(NEW_FonctionContacts::class)->findAll();
  $selectData = array();
        $selectData[0] =" ";
        foreach ($fonction as $res) {
            $selectData[$res->getid()] = $res->getDesignation();
        }
        return $selectData;
}
}
