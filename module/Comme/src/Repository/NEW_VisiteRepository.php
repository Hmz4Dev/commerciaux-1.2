<?php
namespace Comme\Repository;

use Doctrine\ORM\EntityRepository;
use Comme\Entity\NEW_Visite;

/**
 * This is the custom repository class for NEW_Visite entity.
 */
class NEW_VisiteRepository extends EntityRepository
{


    /**
     * Finds all published visites having the given tag.
     * @param integer $idProspect Name of the tag.
     * @return array
     */

    public function getAllvisitesEntreprise($idProspect)
    {
        $entityManager = $this->getEntityManager();

        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('V')
            ->from(NEW_Visite::class, 'V')
            ->Where('V.idProspect = ?2')
            ->orderBy('V.id', 'DESC')
            ->setParameter('2', $idProspect);
        $visitesResult = $queryBuilder->getQuery()->getResult();
        return $visitesResult;
    }


}
