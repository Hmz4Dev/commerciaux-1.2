<?php
namespace Comme\Service\Factory;

use Interop\Container\ContainerInterface;
use Comme\Service\contactsManager;
use Zend\ServiceManager\Factory\FactoryInterface;


class contactsManagerFactory implements FactoryInterface
{

	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

		return new contactsManager($entityManager);
	}
}
