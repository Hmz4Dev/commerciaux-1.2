<?php
namespace Contacts\Service\Factory;

use Interop\Container\ContainerInterface;
use Contacts\Service\familleManager;
use Zend\ServiceManager\Factory\FactoryInterface;


class familleManagerFactory implements FactoryInterface
{
	
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        
		return new familleManager($entityManager);
	}
}