<?php
namespace Comme\Service;

use Comme\Entity\Abstract_Model;
use Comme\Entity\prospects;
use Comme\Entity\famille;
use Zend\Math\Rand;

class prospectsManager
{
	 /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager;
     */

    public $entityManager;



    public function __construct($entityManager)
    {

        $this->entityManager = $entityManager;

    }

     public function gettypecAsString($prospects)
    {
        $code=$prospects->gettypec();
        $typec=$this->entityManager->getRepository(typec::class)->findAll();
    $selectData = array();
        $selectData[0] =" ";
        foreach ($typec as $res) {
            $selectData[$res->getcode()] = $res->gettypecs();
        }
        return $selectData[$code];

    }

     public function getfamilleString($prospects)
    {
        $code=$prospects->getfid();
        if($code==null || $code==0) return 'Aucune';
        $famille=$this->entityManager->getRepository(famille::class)->findAll();
    $selectData = array();
        $selectData[0] =" ";
        foreach ($famille as $res) {
            $selectData[$res->getfid()] = $res->getfamille();

        }
        return $selectData[$code];

    }


    public function save($data)
       {
		$prospects=new prospects();
        $prospects->setOptions($data);
     	$id = $prospects->getid();
     		try{
     		    $datenow=date('Y-m-d H:i:s');

			if (empty($id) || $id=='0') {
                $prospects->setOptions($data);
				$prospects->setCreeLe($datenow);
                if($prospects->getcp()=='') $prospects->setcp(00000);
			//	$data['CreePar']=Zend_Auth::getInstance()->getIdentity()->Login;


			//	$model->setid($id);

					  // Add the entity to the entity manager.
                  $this->entityManager->persist($prospects);

                     // Apply changes to database.
                  $this->entityManager->flush();

        return $prospects;
			}

		}catch(Exception $e){
		    Khatir_Outils::Log('MapperSaveException.log', $e->getMessage());
			$this->_Erreurs[]=$e->getMessage();
			return false;
		}


    }

 public function removeProspects($prospects)
    {

        $this->entityManager->remove($prospects);

        $this->entityManager->flush();
    }
		public function findprospect($idc)
    {
			$prospects = $this->entityManager->getRepository(prospects::class)
                    ->findprospects($idc);

			return $prospects;
		}

	public function findprospectById($idc)
		{
			$prospects = $this->entityManager->getRepository(prospects::class)
                    ->findprospectsID($idc);

			return $prospects;

		}

}
