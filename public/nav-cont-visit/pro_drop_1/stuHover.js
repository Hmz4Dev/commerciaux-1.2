// Copyright 2006-2007 javascript-array.com

var timeout	= 500;
var closetimer	= 0;
var ddmenuitem	= 0;

// open hidden layer
function mopen(id)
{
	// cancel close timer
	mcancelclosetime();

	// close old layer
	if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';

	// get new layer and show it
	ddmenuitem = document.getElementById(id);
	ddmenuitem.style.visibility = 'visible';

}
// close showed layer
function mclose()
{
	if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';
}

// go close timer
function mclosetime()
{
	closetimer = window.setTimeout(mclose, timeout);
}

// cancel close timer
function mcancelclosetime()
{
	if(closetimer)
	{
		window.clearTimeout(closetimer);
		closetimer = null;
	}
}

// close layer when click-out
document.onclick = mclose;

//hideShow visites/contacts
function toggle_visibility(idc) {
	 var c = document.getElementById(idc);


	 if(c.style.display == 'block')
			c.style.display = 'none';
document.getElementById('list1').style.display = "block";
document.getElementById('list2').style.display ="none";

}

function toggle_visible(idv) {
	 var v = document.getElementById(idv);


	 if(v.style.display == 'block')
			v.style.display = 'none';
			document.getElementById('list2').style.display = "block";

}
